use std::fs::File;
use std::io::{self, prelude::*, BufReader, Lines};
use std::path::Path;

fn main() {
    println!("Select task part (1, 2): ");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to parse input!");
    
    match input.trim() {
        "1" => part1(),
        "2" => part2(),
        _ => println!("Invalid task part!"),
    };
}

fn part1() {
    let lines = read_file();
    let mut max_calories = 0;
    let mut current_calories = 0;
    for line in lines {
        let clean_line = line.unwrap();
        if clean_line.trim() == "" {
            if current_calories > max_calories {
                max_calories = current_calories;
            }
            current_calories = 0;
            continue;
        }
        let calories = clean_line.parse::<i32>().unwrap();
        current_calories += calories;
    }
    if current_calories > max_calories {
        max_calories = current_calories;
    }
    println!("Max calories: {}", max_calories);
}

fn part2() {
    let lines = read_file();
    let mut elves = Vec::new();
    let mut current_calories = 0;
    for line in lines {
        let clean_line = line.unwrap();
        if clean_line.trim() == "" {
            elves.push(current_calories);
            current_calories = 0;
            continue;
        }
        let calories = clean_line.parse::<i32>().unwrap();
        current_calories += calories;
    }
    elves.push(current_calories);
    elves.sort_by(|a, b| b.cmp(a));
    println!("Top calories: {}", elves[0] + elves[1] + elves[2]);
}

fn read_file() -> Lines<BufReader<File>> {
    let path = Path::new("./src/input.txt");
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    let lines = reader.lines();
    return lines;
}