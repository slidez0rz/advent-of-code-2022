use std::collections::HashMap;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Lines};
use std::path::Path;

fn main() {
    println!("Select task part (1, 2): ");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to parse input!");
    
    match input.trim() {
        "1" => part1(),
        "2" => part2(),
        _ => println!("Invalid task part!"),
    };
}

fn part1() {
    let lines = read_file();
    let mut score = 0;
    let mut scores = HashMap::new();
    scores.insert("A", 1);
    scores.insert("B", 2);
    scores.insert("C", 3);
    scores.insert("X", 1);
    scores.insert("Y", 2);
    scores.insert("Z", 3);
    for line in lines {
        let clean_line = line.unwrap();
        let mut parts = clean_line.trim().split_whitespace();
        let move_a = parts.next().unwrap();
        let move_b = parts.next().unwrap();
        let score_move_a = scores.get(move_a).unwrap();
        let score_move_b = scores.get(move_b).unwrap();
        score += score_move_b;
        if score_move_a == score_move_b {
            score += 3;
        } else if move_a == "A" && move_b == "Y" {
            score += 6;
        } else if move_a == "B" && move_b == "Z" {
            score += 6;
        } else if move_a == "C" && move_b == "X" {
            score += 6;
        }
    }
    println!("Score is {}", score);
}

fn part2() {
    let lines = read_file();
    let mut score = 0;
    let mut scores = HashMap::new();
    scores.insert("A", 1);
    scores.insert("B", 2);
    scores.insert("C", 3);
    for line in lines {
        let clean_line = line.unwrap();
        let mut parts = clean_line.trim().split_whitespace();
        let move_a = parts.next().unwrap();
        let mut move_b = "";
        let action_b = parts.next().unwrap();
        if action_b == "Y" {
            move_b = move_a.clone();
        } else if action_b == "X" {
            move_b = match move_a {
                "A" => "C",
                "B" => "A",
                "C" => "B",
                _ => ""
            };
        } else if action_b == "Z" {
            move_b = match move_a {
                "A" => "B",
                "B" => "C",
                "C" => "A",
                _ => ""
            };
        }
        let score_move_a = scores.get(move_a).unwrap();
        let score_move_b = scores.get(move_b).unwrap();
        score += score_move_b;
        if score_move_a == score_move_b {
            score += 3;
        } else if move_a == "A" && move_b == "B" {
            score += 6;
        } else if move_a == "B" && move_b == "C" {
            score += 6;
        } else if move_a == "C" && move_b == "A" {
            score += 6;
        }
    }
    println!("Score is {}", score);
}

fn read_file() -> Lines<BufReader<File>> {
    let path = Path::new("./src/input.txt");
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    let lines = reader.lines();
    return lines;
}