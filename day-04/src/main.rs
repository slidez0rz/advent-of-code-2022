use std::fs::File;
use std::io::{self, prelude::*, BufReader, Lines};
use std::path::Path;

fn main() {
    println!("Select task part (1, 2): ");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to parse input!");
    
    match input.trim() {
        "1" => part1(),
        "2" => part2(),
        _ => println!("Invalid task part!"),
    };
}

fn part1() {
    let mut overlaps = 0;

    let lines = read_file();
    for line in lines {
        let clean_line = line.unwrap();
        let line_parts: Vec<&str> = clean_line.split_terminator(',').collect();
        let elf_1_sections: Vec<i32> = line_parts[0].split_terminator('-').map(|s| s.parse::<i32>().unwrap()).collect();
        let elf_2_sections: Vec<i32> = line_parts[1].split_terminator('-').map(|s| s.parse::<i32>().unwrap()).collect();
        if (elf_1_sections[0] <= elf_2_sections[0] && elf_1_sections[1] >= elf_2_sections[1]) || (elf_2_sections[0] <= elf_1_sections[0] && elf_2_sections[1] >= elf_1_sections[1]) {
            overlaps += 1;
        }
    }
    println!("Total number of overlaps is {}", overlaps);
}

fn part2() {
    let mut overlaps = 0;

    let lines = read_file();
    for line in lines {
        let clean_line = line.unwrap();
        let line_parts: Vec<&str> = clean_line.split_terminator(',').collect();
        let elf_1_sections: Vec<i32> = line_parts[0].split_terminator('-').map(|s| s.parse::<i32>().unwrap()).collect();
        let elf_2_sections: Vec<i32> = line_parts[1].split_terminator('-').map(|s| s.parse::<i32>().unwrap()).collect();
        if (elf_1_sections[0] <= elf_2_sections[1] && elf_1_sections[1] >= elf_2_sections[0]) || (elf_2_sections[0] <= elf_1_sections[1] && elf_2_sections[1] >= elf_1_sections[0]) {
            overlaps += 1;
        }
    }
    println!("Total number of overlaps is {}", overlaps);
}

fn read_file() -> Lines<BufReader<File>> {
    let path = Path::new("./src/input.txt");
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    let lines = reader.lines();
    return lines;
}