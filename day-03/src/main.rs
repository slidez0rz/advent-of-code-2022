use std::collections::HashMap;
use std::fs::File;
use std::io::{self, prelude::*, BufReader, Lines};
use std::path::Path;

fn main() {
    println!("Select task part (1, 2): ");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to parse input!");
    
    match input.trim() {
        "1" => part1(),
        "2" => part2(),
        _ => println!("Invalid task part!"),
    };
}

fn part1() {
    let mut score = 0;
    let mut scores: HashMap<char, i32> = HashMap::new();
    let lower_letters: Vec<u8> = (b'a'..=b'z').collect();
    for (i, c) in lower_letters.iter().map(|&c| c as char).enumerate() {
        scores.insert(c, (i + 1).try_into().unwrap());
    }
    let upper_letters: Vec<u8> = (b'A'..=b'Z').collect();
    for (i, c) in upper_letters.iter().map(|&c| c as char).enumerate() {
        scores.insert(c, (i + 27).try_into().unwrap());
    }

    let lines = read_file();
    for line in lines {
        let clean_line = line.unwrap();
        let line_length = clean_line.len();
        let part_length = line_length / 2;
        let first_part = &clean_line[..part_length];
        let second_part = &clean_line[part_length..];
        let mut checked_letters: HashMap<char, bool> = HashMap::new();
        for letter in first_part.chars() {
            if checked_letters.contains_key(&letter) {
                continue;
            }
            checked_letters.insert(letter, true);
            if !second_part.chars().into_iter().find(|&c| c == letter).is_none() {
                if scores.contains_key(&letter) {
                    score += scores.get(&letter).unwrap();
                }
                break;
            }
        }
    }
    println!("Total sum is {}", score);
}

fn part2() {
    let mut score = 0;
    let mut scores: HashMap<char, i32> = HashMap::new();
    let lower_letters: Vec<u8> = (b'a'..=b'z').collect();
    for (i, c) in lower_letters.iter().map(|&c| c as char).enumerate() {
        scores.insert(c, (i + 1).try_into().unwrap());
    }
    let upper_letters: Vec<u8> = (b'A'..=b'Z').collect();
    for (i, c) in upper_letters.iter().map(|&c| c as char).enumerate() {
        scores.insert(c, (i + 27).try_into().unwrap());
    }

    let lines = read_file();
    let lines_vec: Vec<String> = lines.map(|l| l.unwrap()).collect();
    let iter = lines_vec.chunks_exact(3);
    for group in iter {
        let mut common_letters: HashMap<char, i32> = HashMap::new();
        for line in group {
            let mut checked_letters: HashMap<char, bool> = HashMap::new();
            for letter in line.chars() {
                if checked_letters.contains_key(&letter) {
                    continue;
                }
                checked_letters.insert(letter, true);
                if common_letters.contains_key(&letter) {
                    *common_letters.get_mut(&letter).unwrap() += 1;
                } else {
                    common_letters.insert(letter, 1);
                }
            }
        }
        let common_letter = find_key_for_value(&common_letters, 3).unwrap();
        if scores.contains_key(common_letter) {
            score += scores.get(common_letter).unwrap();
        }
    }
    println!("Total sum is {}", score);
}

fn read_file() -> Lines<BufReader<File>> {
    let path = Path::new("./src/input.txt");
    let file = File::open(&path).expect("Failed to open file");
    let reader = BufReader::new(file);
    let lines = reader.lines();
    return lines;
}

fn find_key_for_value<'a>(map: &'a HashMap<char, i32>, value: i32) -> Option<&'a char> {
    map.iter()
        .find_map(|(key, &val)| if val == value { Some(key) } else { None })
}